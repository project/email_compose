<?php

/**
 * @file
 * Contains \Drupal\email_compose\Controller\EmailComposeController.
 */

namespace Drupal\email_compose\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query;
use Drupal\Component\Serialization\Json;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * {@inheritdoc}
 *
 * Defines a controller to manage Email Notification.
 */
class EmailComposeController extends ControllerBase {

  /**
   * {@inheritdoc}
   *
   * Custom function to send Email.
   */
  public function emailComposeSendEmail($content) {
    $mailManager = \Drupal::service('plugin.manager.mail');
    $module = 'email_compose';
    $key = 'email_compose';
    $to = $content['to'];
    $params['message'] = $content['message'];
    $params['subject'] = $content['subject'];
    $params['from'] = $content['from'];
    if (!empty($content['cc'])) {
      $params['headers']['Cc'] = $content['cc'];
    }
    $langcode = \Drupal::currentUser()->getPreferredLangcode();
    $send = true;
    $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);
    if ($result['result'] !== true) {
      $message = t('There was a problem sending your email notification to @email with the subject @title.', array('@email' => $to, '@title' => $content['subject']));
      \Drupal::logger('mail')->error($message);
    }
    else {
      $message = t('An email notification has been sent to @email with the subject @title.', array('@email' => $to, '@title' => $content['subject']));
      \Drupal::logger('mail')->notice($message);
    }
  }
}
