<?php

/**
 * @file
 * Contains Drupal\email_compose\EmailComposeForm
 */

namespace Drupal\email_compose\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Drupal\email_compose\Controller\EmailComposeController;

class EmailComposeForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'email_compose_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = array();
    $users = $this->getAllUsers();
    $form['message_to'] = array(
      '#type' => 'select',
      '#title' => t('To:'),
      '#options' => $users ,
      '#multiple' => TRUE,
      '#default_value' => 0,
    );
    $form['message_to_cc'] = array(
      '#type' => 'select',
      '#title' => t('Cc:'),
      '#options' => $users ,
      '#multiple' => TRUE,
    );
    $form['subject'] = array(
      '#type' => 'textfield',
      '#title' => t('Subject'),
      '#required' => TRUE,
    );
    $form['message'] = array(
      '#type' => 'textarea',
      '#title' => t('Message'),
      '#required' => TRUE,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Send'),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $to = $cc = '';
    $to_users = $form_state->getValue('message_to');
    foreach ($to_users as $to_user) {
      $to .= $to_user . ',';
    }
    $content['to'] = $to;

    if (!empty($form_state->getValue('message_to_cc'))) {
      $message_to_cc = $form_state->getValue('message_to_cc');
      foreach ($message_to_cc as $to_cc) {
        $cc .= $to_cc . ',';
      }
      $content['cc'] = $cc;
    }
    $subject = $form_state->getValue('subject');
    $message = $form_state->getValue('message');
    $content['subject'] = $subject;
    $content['message'] = $message;
    $message_notification = new EmailComposeController();
    $mail_send = $message_notification->emailComposeSendEmail($content);
  }

  /**
   * Custom function to get all users.
   */
  public function getAllUsers($uids = NULL) {
    if (empty($uids)) {
      $query = \Drupal::entityQuery('user');
      $query->condition('status', 1);
      $uids = $query->execute();
    }
    $accounts = \Drupal::service('entity_type.manager')->getStorage('user')->loadMultiple($uids);
    foreach ($accounts as $account) {
      $users[$account->get('mail')->value] = $account->get('name')->value;
    }
    return $users;
  }
}
